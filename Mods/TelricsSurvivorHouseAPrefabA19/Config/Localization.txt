Key,english,japanese

----------------Items----------------------
questXaviersMail,"[e7a729]Unopened Parcel[-]","[e7a729]Japanese[-]"
questXaviersMailDesc,"[e7a729]Seems someone never opened their mail.[-]","[e7a729]Japanese[-]"

resourceXaviersRemains,"[e7a729]Xavier's Remains[-]","[e7a729]Japanese[-]"
resourceXaviersRemainsDesc,"[e7a729]He didn't make it. The person that sent the parcel might be interested to know he's no longer with us...[-]","[e7a729]Japanese[-]"
resourceFindXavier,"[e7a729]Find Xavier[-]","[e7a729]Japanese[-]"
resourceFindXavierDesc,"[e7a729]Found Xavier[-]","[e7a729]Japanese[-]"



----------------Blocks----------------------
XaviersMailbox,"[e7a729]Xavier's Mailbox[-]","[e7a729]Japanese[-]"
XaviersMailboxDesc,"[e7a729]It's jammed! Break it to open.[-]","[e7a729]Japanese[-]"

XaviersAlive,"[e7a729]Xavier[-]","[e7a729]Japanese[-]"
XaviersAliveDesc,"[e7a729]Xavier[-]","[e7a729]Japanese[-]"
XaviersRemains,"[e7a729]Xavier[-]","[e7a729]Japanese[-]"
XaviersRemainsDesc,"[e7a729]Xavier[-]","[e7a729]Japanese[-]"
XaviersCoffin,"[e7a729]Xavier's Coffin[-]","[e7a729]Japanese[-]"
XaviersCoffinDesc,"[e7a729]Xavier's Coffin[-]","[e7a729]Japanese[-]"

------------------Quests--------------------
questXaviersMailStart,"[e7a729]Unopened Parcel[-]","[e7a729]Japanese[-]"
questXaviersMailStart_offer_title,"[e7a729]Unopened Parcel[-]","[e7a729]Japanese[-]"
questXaviersMailStart_subtitle,"[e7a729]Unopened Parcel[-]","[e7a729]Japanese[-]"
questXaviersMailStart_offer,"[e7a729]Hello Xavier. Hope you're surviving this apocalypse the best you can. Here is the payment for completing your job. Sorry it took so long to get to you. See you soon![-]","[e7a729]Japanese[-]"

questXaviersMail1,"[e7a729]Unopened Parcel[-]","[e7a729]Japanese[-]"
questXaviersMail1_offer_title,"[e7a729]Unopened Parcel[-]","[e7a729]Japanese[-]"
questXaviersMail1_subtitle,"[e7a729]Unopened Parcel[-]","[e7a729]Japanese[-]"
questXaviersMail1_offer,"[e7a729]*Xavier coughs a few times* They swarmed us. They killed everyone here. Even the animals. There was no stopping them. *You show Xavier the package* What's this? *Cough* This won't help me now, friend. If I'm left here any longer, I'll turn as well. You need to do it. *Cough cough* End it now and tell one of Joel's traders I went down fighting. *Cough*[-]","[e7a729]Japanese[-]"


questXaviersMail2,"[e7a729]Unopened Parcel[-]","[e7a729]Japanese[-]"
questXaviersMail2_offer_title,"[e7a729]Unopened Parcel[-]","[e7a729]Japanese[-]"
questXaviersMail2_subtitle,"[e7a729]Unopened Parcel[-]","[e7a729]Japanese[-]"
questXaviersMail2_offer,"[e7a729]*As Xavier dies, he gives you one last nod of approval. It's probably a good idea to get to the nearest trader area and return this package.*[-]","[e7a729]Japanese[-]"


questXaviersMail3,"[e7a729]Unopened Parcel[-]","[e7a729]Japanese[-]"
questXaviersMail3_offer_title,"[e7a729]Unopened Parcel[-]","[e7a729]Japanese[-]"
questXaviersMail3_subtitle,"[e7a729]Unopened Parcel[-]","[e7a729]Japanese[-]"
questXaviersMail3_offer,"[e7a729]*As you talk to the trader, you explain what happened with Xavier* Wow. Xavier's gone... It's a shame such a tough guy got hit that hard with a horde... But atleast you kept him from turning into one of them himself. You did a good thing, but it's not over. Xavier needs to have a proper burial. Find some way to bury him. Come back when its done and we'll sort you out with some form of payment for helping.[-]","[e7a729]Japanese[-]"


questXaviersMail4,"[e7a729]Unopened Parcel[-]","[e7a729]Japanese[-]"
questXaviersMail4_offer_title,"[e7a729]Unopened Parcel[-]","[e7a729]Japanese[-]"
questXaviersMail4_subtitle,"[e7a729]Unopened Parcel[-]","[e7a729]Japanese[-]"
questXaviersMail4_offer,"[e7a729]*On placing the coffin on the ground, you stick around and say a few words about this brave man you never knew. Even though your first interaction was you killing him, you somehow feel like you knew him all your life. Atleast he's buried properly now. Get back to the trader to let them know.*[-]","[e7a729]Japanese[-]"