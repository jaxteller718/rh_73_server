=== BOID'S INFINITE HORDE MODLET
=== v1.0, 9/8/2020

WHAT THIS MOD DOES
- Alters the final (hardest) wave of the Blood Moon Horde so it spawns up to 99,999 zombies (limited by the per-player settings of both the horde wave and your game settings)
- Guarantees you will be fighting until 4AM!

INSTALLATION
- Unzip into the /mods folder so you will have:

<7D2D install folder>/mods/BoidsInfiniteHorde
   ModInfo.xml
   /Config
     gamestages.xml
     
HOW TO USE IN GAME
- Wait for Blood Moon Horde
- Never. Stop. Fighting.