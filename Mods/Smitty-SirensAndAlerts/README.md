**For game**: [7 Days to Die](https://7daystodie.com)

**Description:**
This is an attempt to make changes to the vanilla sounds to make the game environment
more "diverse" or "immersive" by changing the pitch or allowed pitch ranges.  

This is *also* an attempt to come up with more "normal sounding" extra game sounds by
remixing common sounds heard in the game in order to make repetitive/common
actions have a little more variety and increase immersion.

If you hear a sound that's a little too "weird" let me know!
Please reference the item/action that causes the sound.

Currently this mod adds extra sounds to these additional items.

| Item/block          | Action             | Added sounds  | Added in version  |
|:------------------- |:------------------:| -------------:|-------------:|
| wrench              | Harvesting         | 9             | v1.0.0 |
| backpacks(container)| Opening+Closing    | 8             | v1.0.0 |
| cupboards           | Opening+Closing    | 14            | v1.0.0 |
| stone               | Destroying         | 6             | v1.2.0 |
| wood                | Destroying         | 7             | v1.2.0 |
| metal               | Destroying         | 2             | v1.2.0 |
| cloth               | Destroying         | 3             | v1.2.0 |
| door(wood)          | Opening+Closing    | 10            | v1.2.0 |
| door(metal)         | Opening+Closing    | 12            | v1.2.0 |


**Tested on**:

| Game version tested on | This Mod version  | Note |
| :------------ | :------------- | :------------- |
| alpha 19.2 b3 | v1.2.0 | Likely works with all a19+ |
| alpha 19.2 b3 | v1.1.0 | Likely works with all a19+ |
| alpha 19 B157 | v1.0.0 | Likely works with all a19+ |
| alpha 18.3 B3 | v1.0.0 | Likely works with all a18+ |

**To install game mods**: [7 Days to Die modlet installation](https://gist.github.com/doughphunghus/a1907c5f63b5fe79bd823965328f25bf)

**Discussions (and additional info, links, etc)**: [My modlets main page](https://community.7daystodie.com/topic/17197-doughs-modlets/)

**Modding Terms of Agreement**: [TFP Official Modding Forum Policy ](https://community.7daystodie.com/topic/4189-tfp-official-modding-forum-policy/)
